package com.jahid.practice_program;

public class PrimeNumber {

    public static void main(String[] args) {
        int num = 13;
        System.out.println(isPrime(num));

        // printing prime number between 100 and 999
        int end = 999;
        for (int start = 100; start <= end; start++) {
            if (isPrime(start)) {
                System.out.println(start);
            }

        }
    }

    static boolean isPrime(int num) {
        boolean isPrime = true;
        for (int i = 2; i < num; i++) {
            if (num % i == 0) {
                isPrime = false;
                break;
            }
        }
        return isPrime;
    }
}
