package com.jahid.practice_program;

import java.util.Arrays;

public class VarArgs {
    public static void main(String[] args) {
        printInteger(1,3,1,2,4,1,31,13,1,5,7767,45);
    }

     static void printInteger(int ...num) {
         System.out.println(Arrays.toString(num));
    }
}
