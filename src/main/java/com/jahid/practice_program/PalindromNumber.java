package com.jahid.practice_program;

//A palindromic number is a number that remains the same when its digits are reversed.

public class PalindromNumber {

    public static void main(String[] args) {
        System.out.println(isPalindromNumber(1531));
    }

    static boolean isPalindromNumber(int number) {
        int reverse = 0;
        int originalNumber = number;
        while (number > 0) {
            int rem = number % 10;
            number = number / 10;
            reverse = (reverse * 10) + rem;
        }

        if (reverse == originalNumber) {
            return true;
        }

        return false;
    }
}
