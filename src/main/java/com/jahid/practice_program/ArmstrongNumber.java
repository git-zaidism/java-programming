package com.jahid.practice_program;

//An Armstrong number, is a number that is equal to the sum of the cubes of its own digits.
// For example, 370 is an Armstrong number since 370 = 3*3*3 + 7*7*7 + 0*0*0. and 153 is also armstrong number

public class ArmstrongNumber {

    public static void main(String[] args) {
        //printing all 3 digit armstrong number
        for (int i = 100; i <= 999; i++) {
            if (isArmstrongNumber(i)) {
                System.out.print(i + " ");
            }
        }
        // checkin number is armstring or not
        System.out.println(isArmstrongNumber(120));
    }

    static boolean isArmstrongNumber(int num) {
        int number = num;
        boolean isArmstrong = false;
        int sum = 0, cube = 0;
        while (num > 0) {
            int rem = num % 10;
            cube = rem * rem * rem;
            sum = sum + cube;
            num = num / 10;
        }
        if (number == sum)
            return true;

        return false;
    }

}
