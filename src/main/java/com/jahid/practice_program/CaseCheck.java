package com.jahid.practice_program;

import java.util.Scanner;

public class CaseCheck {

    public static void main(String[] args) {
        caseCheck();
    }

     static void caseCheck() {
         Scanner input = new Scanner(System.in);
         System.out.print("Enter a Alphabet: ");
         char ch = input.next().trim().charAt(0);

         if (ch >= 'a' && ch <= 'z')
             System.out.println("It's Lower Case");
         else
             System.out.println("It's Upper Case");
    }
}
