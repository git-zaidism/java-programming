package com.jahid.practice_program;

public class OccurrenceNumber {

    public static void main(String[] args) {

        int number = 22732482;
        int numToBeCount = 2;
        int count = 0;

        while (number > 0) {
            int temp = number % 10;
            if (temp == numToBeCount) {
                count++;
            }
            number = number / 10;
        }
        System.out.println("Occurence of " + numToBeCount + " is " + count + " times.");
    }
}
