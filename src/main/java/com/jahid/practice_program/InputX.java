package com.jahid.practice_program;

//Keep taking numbers as inputs till the user enters ‘0’, after that print sum of all.

import java.util.Scanner;

public class InputX {

    public static void main(String[] args) {
        int sum = 0;
        int n;
        Scanner input = new Scanner(System.in);

        while (true) {
            System.out.println("Enter Number: ");
            n = input.nextInt();
            sum = sum + n;
            if (n == 0)
                break;
        }
        System.out.println(sum);
    }
}
