package com.jahid.practice_program;

import jdk.swing.interop.SwingInterOpUtils;

import java.util.Scanner;

public class Fibonacci {

    public static void main(String[] args) {
        fibonacci(10);
    }

    static void fibonacci(int nth) {

        int num1 = 0, num2 = 1;
        int temp;

        System.out.println(num1);
        System.out.println(num2);

        int count = 3;
        while (count <= nth) {
            temp = num1 + num2;
            System.out.println(temp);
            num1 = num2;
            num2 = temp;
            count++;
        }
    }
}
