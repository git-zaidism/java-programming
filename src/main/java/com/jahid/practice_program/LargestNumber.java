package com.jahid.practice_program;

public class LargestNumber {

    public static void main(String[] args) {

        // approach 1
        int a = 10, b = 187, c = 101;
        int max = a;

        if (b > a)
            max = b;
        if (c > b)
            max = c;

        System.out.println("Max is : " + max);

        // another approach using Math.max()
        int maximum = Math.max(Math.max(a, b), c);
        System.out.println("Max is using Math.max() : " + maximum);
    }
}
