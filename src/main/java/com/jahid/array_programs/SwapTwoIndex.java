package com.jahid.array_programs;

import java.util.Arrays;

public class SwapTwoIndex {
    public static void main(String[] args) {
        int[] arr = {1, 4, 9, 0, 4, 56, 22, 37};
        SwapUtil swapObj = new SwapUtil();
        // swapping index 0 element with index 3
        swapObj.swap(arr, 0, 3);
        System.out.println(Arrays.toString(arr));
    }

}
