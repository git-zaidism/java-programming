package com.jahid.array_programs;

public  class SwapUtil {

    // swapping two elements of array at specified index
    int[] swap(int arr[], int index1, int index2) {
        int temp;
        temp = arr[index1];
        arr[index1] = arr[index2];
        arr[index2] = temp;
        return arr;
    }

}
