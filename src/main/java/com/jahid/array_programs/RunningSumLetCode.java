package com.jahid.array_programs;
/*
Input:nums=[1,2,3,4]
        Output:[1,3,6,10]
        Explanation:Running sum is obtained as follows:[1,1+2,1+2+3,1+2+3+4].
*/

import java.util.Arrays;

public class RunningSumLetCode {
    public static void main(String[] args) {
        int[] nums = {1, 2, 3, 4};
        System.out.println(Arrays.toString(runningSum(nums)));
    }

    private static int[] runningSum(int[] nums) {

        for (int i = 1; i < nums.length; i++) {
            nums[i] = nums[i] + nums[i - 1];
        }
        return nums;
    }
}
