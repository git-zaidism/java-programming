package com.jahid.array_programs;


public class MaximumInArray {

    public static void main(String[] args) {
        int[] arr = {1, 4, 98, 0, 4, 56, 22, 37, 99};
        System.out.println(maximum(arr));
    }

    static int maximum(int[] arr) {
        int max = arr[0];
        for (int element : arr) {
            if (element > max)
                max = element;
        }
        return max;
    }


}
