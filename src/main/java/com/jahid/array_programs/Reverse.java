package com.jahid.array_programs;

import java.util.Arrays;

public class Reverse {

    public static void main(String[] args) {
        int[] arr = {1, 4, 9, 0, 4, 56, 22, 37};
        int start = 0;
        int end = arr.length - 1;
        SwapUtil swapObj = new SwapUtil();

        while (start < end) {
            swapObj.swap(arr, start, end);
            start++;
            end--;
        }
        System.out.println(Arrays.toString(arr));

    }
}
