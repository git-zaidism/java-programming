package com.jahid.array_programs;

import java.util.Arrays;
import java.util.Scanner;

public class Two_D_Array {

    public static void main(String[] args) {

        // mentioning col size in 2d array is optional as col can be varied in size in every sub array
        int arr1[][] = new int[3][3];

//        int arr[][] = {
//                {1, 2, 3},
//                {4, 5},
//                {6, 7, 8, 9}
//        };
        Scanner input = new Scanner(System.in);
        // 2-d array input
        System.out.println("Enter values for 2-D Array: ");
        for (int row = 0; row < arr1.length; row++) { //, length give you number of row
            for (int col = 0; col < arr1[row].length; col++) {
                arr1[row][col] = input.nextInt();
            }
        }
        // 2-d array output
        for (int row = 0; row < arr1.length; row++) { //, length give you number of row
            for (int col = 0; col < arr1[row].length; col++) {
                System.out.print(arr1[row][col] + " ");
            }
            //after printing every row print new line
            System.out.println();
        }

        // another way of iterating and printing
        for (int[] row : arr1) {
            System.out.println(Arrays.toString(row));
        }
    }

}
