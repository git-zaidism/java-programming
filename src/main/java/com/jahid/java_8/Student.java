package com.jahid.java_8;

import java.util.Objects;

public class Student {

    private int rollNumber;
    private String name;
    private int age;
    private String stream;

    public Student(int rollNumber, String name, int age, String stream) {
        this.rollNumber = rollNumber;
        this.name = name;
        this.age = age;
        this.stream = stream;
    }

    public int getRollNumber() {
        return rollNumber;
    }

    public void setRollNumber(int rollNumber) {
        this.rollNumber = rollNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getStream() {
        return stream;
    }

    public void setStream(String stream) {
        this.stream = stream;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Student)) return false;
        Student student = (Student) o;
        return getRollNumber() == student.getRollNumber() && getAge() == student.getAge() && getName().equals(student.getName()) && getStream().equals(student.getStream());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getRollNumber(), getName(), getAge(), getStream());
    }

    @Override
    public String toString() {
        return "Student{" +
                "rollNumber=" + rollNumber +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", stream='" + stream + '\'' +
                '}';
    }
}
