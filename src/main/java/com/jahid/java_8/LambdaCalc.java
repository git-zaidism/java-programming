package com.jahid.java_8;

import java.util.Scanner;

import javax.management.RuntimeErrorException;

public class LambdaCalc {

	public static void main(String[] args) {
		
		FunInterfaceCalc sub = (num1, num2) -> {
			if (num1 > num2)
				return num1 - num2;
			else
				throw new RuntimeErrorException(null);
		};
		FunInterfaceCalc add = (num1, num2) -> num1 + num2;
		FunInterfaceCalc mul = (num1, num2) -> num1 * num2;
		FunInterfaceCalc div = (num1, num2) -> num1 / num2;
		FunInterfaceCalc mod = (num1, num2) -> num1 % num2;

		System.out.println("Enter the Operation you need to perform: / , * ,- , +, %");
		Scanner me = new Scanner(System.in);
		String s = me.next();
		if (s.equals("+"))
			System.out.println(add.calc(30, 10));
		else if (s.equals("-"))
			System.out.println(sub.calc(30, 10));
		else if (s.equals("*"))
			System.out.println(mul.calc(30, 10));
		else if (s.equals("%"))
			System.out.println(mod.calc(30, 10));
		else if (s.equals("/"))
			System.out.println(div.calc(30, 10));
		else
			System.out.println("Invalid Operation");

	}

}
