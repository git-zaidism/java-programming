package com.jahid.java_8;

@FunctionalInterface
public interface FunInterfaceCalc {

	int calc(int num1, int num2);
}
