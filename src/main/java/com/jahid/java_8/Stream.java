package com.jahid.java_8;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Stream {

	public static void main(String args[]) {
		Stream sObject = new Stream();

		System.out.println(sObject.namesFilter());
		System.out.println(sObject.numberFilteration());

        List<Student> distinctStudentList=sObject.distinctWithObject();
        distinctStudentList.forEach(student -> System.out.println(student));
	}



    // Names starts with any alphabet except 'A'
	public List<String> namesFilter() {
		List<String> names = List.of("Zahid", "Ankit", "Harish", "Shankar", "Arpita", "Aryan");
		// predicate need to pass inside filter
		List<String> namesAfterFilter = names.stream().filter(n -> (!n.startsWith("A"))).collect(Collectors.toList());
		return namesAfterFilter;
	}

	// Even Numbers using stream
	public List<Integer> numberFilteration() {
		List<Integer> numbers = List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
		List<Integer> evenNumber = numbers.stream().filter(n -> n % 2 == 0).collect(Collectors.toList());
		return evenNumber;
	}

    // distinct: in order to use distinct on custom object pojo class should have
    // equals and hascode method in it
    private List<Student> distinctWithObject() {
        Student student1 = new Student(1, "Jahid", 20, "CSE");
        Student student2 = new Student(2, "Harish", 21, "ETC");
        Student student3 = new Student(3, "Ankit", 19, "IT");
        Student student4 = new Student(1, "Jahid", 20, "CSE");

        List<Student> studentList = List.of(student1, student2, student3, student4);

        List<Student> distinctStudentList = studentList.stream().distinct().collect(Collectors.toList());

        // another way
        Set<Student> studentSet= new LinkedHashSet<>();
        studentSet.addAll(studentList);

        return distinctStudentList;

    }


}
