/* Default Method are came into picture to enable the functionality of lambda expression in java.
   Java 8 allows us to add non-abstract methods in the interfaces.
   These methods must be declared default methods.
   Default methods were introduced in java 8 to enable the functionality of lambda expression.
   Default methods enable us to introduce new functionality to the interfaces of our libraries
   and ensure binary compatibility with code written for older versions of those interfaces.
*/
package com.jahid.java_8;

public interface InterfaceWithDefaultMethod {
	default void print1() {
		System.out.println("Java 8 allows us to add non-abstract methods in the interfaces");
	}

	default void print2() {
		System.out.println("Java 8 allows us to add non-abstract methods in the interfaces");
	}

	static void print() {
		System.out.println("Java 8 allows us to add staticnon-abstract methods in the interfaces");

	}
}
